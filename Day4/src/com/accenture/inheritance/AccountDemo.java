package com.accenture.inheritance;
public class AccountDemo {
	public static void main(String[] args) {
		Account sa = new SavingAccount(5001, "saving", 10000, 5);
		Account fd = new FdAccount(102, "FD", 25000, 6, 5);
		//Account ac = new Account(103, "sd", 234);
		System.out.println(sa);
		System.out.println(fd);
		
		sa.calculateInterest();
		fd.calculateInterest();
	}

}
