package com.accenture.inheritance;

public class SavingAccount extends Account {
	private int roi;

	public SavingAccount() {
		super();	//it is calling parent constructor
	}
	
	public SavingAccount(int accNo, String accType, int balance, int roi) {
		super(accNo, accType, balance);	//it will parent constructor
		this.roi = roi;
	}

	public int getRoi() {
		return roi;
	}

	public void setRoi(int roi) {
		this.roi = roi;
	}

	@Override
	public String toString() {
		return "SavingAccount [roi=" + roi + ", getAccNo()=" + getAccNo() + ", getAccType()=" + getAccType()
				+ ", getBalance()=" + getBalance() + "]";
	}
	
	@Override
	public void calculateInterest() {
		System.out.println(getBalance()*roi/100);
	}

}
