package com.accenture.inheritance;

public abstract class Account extends Object{
	private int accNo;
	private String accType;
	private int balance;
	
	public Account() {
		super();
	}
	public Account(int accNo, String accType, int balance) {
		super();
		this.accNo = accNo;
		this.accType = accType;
		this.balance = balance;
	}
	public int getAccNo() {
		return accNo;
	}
	public void setAccNo(int accNo) {
		this.accNo = accNo;
	}
	public String getAccType() {
		return accType;
	}
	public void setAccType(String accType) {
		this.accType = accType;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "Account [accNo=" + accNo + ", accType=" + accType + ", balance=" + balance + "]";
	}
	public abstract void calculateInterest();		
	//we are creating contract among many subclasses, now it is mandatory to implement this method by all subclasses
}
