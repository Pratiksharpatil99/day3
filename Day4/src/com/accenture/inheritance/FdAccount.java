package com.accenture.inheritance;

public class FdAccount extends Account {
	//no need to create accno, acctype,balance as it is inherited from parent. reusability
	private int roi;
	private int duration;

	public FdAccount() {
		super();
	}

	public FdAccount(int accNo, String accType, int balance, int roi, int duration) {
		super(accNo, accType, balance);
		this.roi = roi;
		this.duration = duration;
	}

	public int getRoi() {
		return roi;
	}

	public void setRoi(int roi) {
		this.roi = roi;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		return "FDAccount [roi=" + roi + ", duration=" + duration + ", getAccNo()=" + getAccNo() + ", getAccType()="
				+ getAccType() + ", getBalance()=" + getBalance() + "]";
	}

	@Override
	public void calculateInterest() {
		System.out.println((getBalance() * roi * duration)/100);
	}
}

